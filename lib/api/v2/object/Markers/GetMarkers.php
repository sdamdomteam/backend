<?php
namespace api\v2\object\Markers;

use \RedBeanPHP\R as R;
use api\Reader\Read;
use api\JsonEncode\Encode;
/**
 * Created by PhpStorm.
 * User: repla
 * Date: 30.05.2018
 * Time: 23:28
 */
class GetMarkers {
    public $filter;
    public $typeDealId = 1;
    public $typeFloatId = 3;
    public $id = array();
    public $inputApi;

    public function __construct(Read $inputApi)
    {
        $this->inputApi = $inputApi->authData;
    }

    public function getMarkers(Encode $encode) {
        $this->verifyFilter();
        $this->getIds();
        $encode->default = $this->id;
    }

    public function verifyFilter() {
        if (!$this->inputApi['activePlaceId']) {
            throw new \RuntimeException('Город не выбран', ACCESS_DENIED);
        }
        if ( !$this->inputApi['typeFloat']) {
            throw new \RuntimeException('Тип помещения не выбран', ACCESS_DENIED);
        }
        if ( !$this->inputApi['typeDeal']) {
            throw new \RuntimeException('Тип сделки не выбран', ACCESS_DENIED);
        }
    }

    public function getIds() {
        $ids = R::findLike( $this->inputApi['activePlaceId'], [
            'type_deal_id' => $this->inputApi['typeDeal'], 'type_float_id' => $this->inputApi['typeFloat']
        ], ' ORDER BY id LIMIT 100 ' );
        if (!$ids) {
            throw new \RuntimeException('По Вашему запросу объявлений не найдено :(', ACCESS_DENIED);
        }
        foreach ($ids as $obj) {
            $this->id[] = ['id'=>(int)$obj->id,'position'=>['latitude'=>$obj->lat,'longitude'=>$obj->lng],'massage'=>$obj->massage,'price'=>((string)$obj->price)/1000 .'K'];
        }
    }
}