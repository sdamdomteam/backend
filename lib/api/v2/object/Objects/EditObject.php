<?php

namespace api\v2\object\Objects;

use api\Reader\Read;
use api\JsonEncode\Encode;
use RedBeanPHP\R as R;

//3 jenkins commit
/**
 * Created by PhpStorm.
 * User: repla
 * Date: 15.06.2018
 * Time: 9:31
 */
class EditObject
{
    public $object;
    public $inputApi;
    public $id;

    public function __construct(Read $inputApi)
    {
        $this->inputApi = $inputApi->authData;
    }

    public function loadObject(Encode $encode)
    {
        $this->object = R::load(OBJECT_TABLE, $this->inputApi['objectId']);
        $encode->default = $this->object;
    }

    public function saveObject()
    {
        $this->object->massage = $this->inputApi['massage'];
        foreach ($this->inputApi['facilities'] as $id) { // Доступно как dump($float->ownFatList);
            $fat = R::dispense('fat');
            $fat->facilities = R::findOne('facilities',$id);
            $fat->object = $this->object;
            $this->object->ownFatList[] = $fat;
        }
        $this->object = $this->inputApi['price'];
        $this->id = R::store($this->object);
    }
}