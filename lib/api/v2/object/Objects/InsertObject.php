<?php
namespace api\v2\object\Objects;
use api\JsonEncode\Encode;
use api\Reader\Read;
use \RedBeanPHP\R as R;
/**
 * Created by PhpStorm.
 * User: repla
 * Date: 08.06.2018
 * Time: 2:35
 */

class InsertObject {
    private $userTable = 'user';
    private $objectTable = 'object';
    public $city;
    public $id;
    public $photoNamesArray = [];
    public $photos = ['0'=>'wmrvewjk.jpg','1'=>'snfd4rbp.jpg','2'=>'kzbweysg.jpg'];//хз как сделоать зависимости
    public $facilities = [1,2,3]; //так приходят с фронта, бины получаю строкой ниже, зависимости сделать не понимаю как
    //$facilities = R::loadAll('facilities', [1,2,3]);
    /*Класс работает с добавлением объявлений*/
    //Пример входного массива
    //Фото one to many(??)

    public function __construct(Read $inputApi)
    {
        $this->inputApi = $inputApi->authData;
    }

    public function createObject(Encode $encode) {
        $this->verifyCity();
        $this->savePhoto();
        $this->create();
        $this->ok($encode);

    }

    public function verifyCity() {
        $this->city = R::findOrCreate( 'city', [
            'country' => $this->inputApi['country'],
            'name' => $this->inputApi['cityName'],
            'city_uniq_code' => $this->inputApi['cityUniqCode']] );
    }
    public function savePhoto() {
        $types = array('image/gif', 'image/png', 'image/jpeg', '');
        $path_end = $_SERVER['DOCUMENT_ROOT']."/photo/".mb_strtolower($this->inputApi['cityUniqCode']).'/';
        if (file_exists($path_end)) {
        } else {
            mkdir($path_end, 0700, true);
        }
        for ($i=0; $i < count($_FILES['picture']['name']); $i++) {
            $file_name = md5(microtime() . rand(0, 9999));
            $file_type = $_FILES['picture']['type'][$i];
            $file_size = $_FILES['picture']['size'][$i];
            $file_error = $_FILES['picture']['error'][$i];
            //Копирую картинку
            $file_ext =  strtolower(strrchr($_FILES['picture']['name'][$i],'.'));
            $file_name_new = $file_name.$file_ext;
            $file_name_new_full = $path_end.$file_name_new;
            if (in_array($data['picture']['type'], $types)) { copy ( $_FILES['picture']['tmp_name'][$i], $file_name_new_full ); };
            //$all_photo_name = $all_photo_name.",".$file_name_new;
            $this->photoNamesArray = $file_name_new_full;
        }
    }
    public function create() {
        $user = R::load($this->userTable,$this->inputApi['userId']);
        $float = R::dispense($this->objectTable);
        $float->user = $user;
        $float->city = $this->city;
        $float->typeDeal = R::load('type_deal',$this->inputApi['typeDeal']);
        $float->typeFloat = R::load('type_float',$this->inputApi['typeFloat']);
        $float->massage = $this->inputApi['massage'];
        foreach ($this->inputApi['facilities'] as $id) { // Доступно как dump($float->ownFatList);
            $fat = R::dispense('fat');
            $fat->facilities = R::findOne('facilities',$id);
            $fat->object = $float;
            $float->ownFatList[] = $fat;
        }
        foreach (/*$this->photoNamesArray*/$this->inputApi['photo'] as $item) { // Доступно как dump($float->ownPhotoList);
            $photo = R::dispense('photo');
            $photo->photo = $item;
            $photo->object = $float;
            $float->ownPhotoList[] = $photo;
        }
        $float->price = $this->inputApi['price'];
        $float->priceIn = R::load('price_in',$this->inputApi['priceIn']);
        $float->address = $this->inputApi['address'];
        $float->lat = $this->inputApi['lat'];
        $float->lng = $this->inputApi['lng'];
        $float->date = date(DATE_RFC822);
        $float->moder = false;
        $user->ownObjectList[] = $float; // Выставляем зависимость для получения $user->ownObjectList - список объявлений пользователя
        //$photo = R::dispense('photo'); // Не понимаю как правильно хранить
        $this->id = R::store($float);
        //В RB уже встроены френдли исключения, нужно их перехватить, думаю как
        return true;
    }
    public function ok(Encode $encode) {
        $encode->default = ['status' => true, 'lastCreate' => $this->id];
    }
}