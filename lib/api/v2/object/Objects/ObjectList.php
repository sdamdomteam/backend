<?php
namespace api\v2\Object\Objects;
use api\JsonEncode\Encode;
use api\Reader\Read;
use \RedBeanPHP\R as R;
/**
 * Created by PhpStorm.
 * User: repla
 * Date: 22.05.2018
 * Time: 22:24
 */

class ObjectList {
    /*
     Класс работает с о списком объявлений в БД
    getList($filter) тащит из БД список элементов соответствующих фильтру
    getOne($id) тащит один элемент по ИД
     */
    private $id;
    private $inputApi;

    /**
     * Object constructor.
     * @param Read $inputApi
     */
    public function __construct(Read $inputApi)
    {
        $this->inputApi = $inputApi->authData;
    }

    /**
     * @param $encode
     * @return mixed
     */
    public function getOne(Encode $encode) {
        $this->idValidate();
        $obj  = R::find( 'chijutid7vnvuuerqlewhm_a0yg', ' id = '.$this->inputApi['id'].' ');
        $encode->default = $obj;
        return true;
    }

    public function idValidate() {
        //Проверем введен ли идентификатор
        if (empty($this->inputApi['id']))
        {
            throw new \RuntimeException('Enter ID', UNAUTHORIZED);
        }
        return true;
    }
    /**
     * @return mixed
     */
    public function getList(Encode $encode) {
        //Делаем выборку из БД по входным данным
        $list = R::findLike( 'chijutid7vnvuuerqlewhm_a0yg', [
            'type_float' => $this->inputApi
        ], ' ORDER BY type_float limit 20 ' );
        $encode->default = $list;
        return true;
    }
}