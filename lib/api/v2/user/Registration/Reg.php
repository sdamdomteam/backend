<?php

namespace api\v2\user\Registration;
use api\Reader\Read;
use api\JsonEncode\Encode;
use Respect\Validation\Validator as v;
use \RedBeanPHP\R as R;

class Reg
{
    private $login;
    private $password;

    /**
     * Reg constructor.
     * @param $data
     * @throws \Exception
     */
    public function __construct(Read $data)
    {
        if ($data->authData['repPassword'] !== $data->authData['password']) {
            throw new \RuntimeException('Введенные пароли не совпадают', UNAUTHORIZED);
        }
        $this->login = $data->authData['login'];
        $this->password = $data->authData['password'];
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function registration(Encode $encode)
    {
        $this->loginValidate();
        $encode->default = $this->doReg();
    }


    /**
     * @return string
     * @throws \Exception
     */
    public function loginValidate()
    {

        if (!v::email()->validate($this->login)) {
            throw new \RuntimeException('Не верный формат логина / Не является email адресом', ACCESS_DENIED);
        }
        return true;
    }

    private function doReg()
    {
        $user = R::dispense( USER_TABLE );
        $user->first_name = 'Test first_name';
        $user->last_name = 'Test last_name';
        $user->vk_id = 'none';
        $user->user_type = 1;
        $user->login = $this->login;
        $user->password = password_hash($this->password, PASSWORD_DEFAULT);
        $id = R::store( $user ); //В переменной будет храниться идентификатор пользователя под которым он был записан
        return ['status' => 'Success Reg', 'login' => $this->login, 'YourID' => $id];
        /*Тут будем регистрировать*/
    }
}