<?php
namespace api\v2\user\Profile;

use api\Reader\Read;
use api\JsonEncode\Encode;
use \RedBeanPHP\R as R;
/**
 * Created by PhpStorm.
 * User: repla
 * Date: 15.06.2018
 * Time: 10:46
 */

class GetProfile {
    public $id;
    public function __construct(Read $inputApi)
    {
        $this->id = $inputApi['userId'];
    }
    public function get(Encode $encode) {
        $user = R::load(USER_TABLE,$this->id);
        $encode->default = $user;
    }
}