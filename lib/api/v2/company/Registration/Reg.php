<?php

namespace api\v2\company\Registration;
use api\JsonEncode\Encode;
use api\Reader\Read;
use \RedBeanPHP\R as R;
/**
 * Created by PhpStorm.
 * User: repla
 * Date: 15.06.2018
 * Time: 10:00
 */
class Reg
{
    public $inputApi;

    public function __construct(Read $inputApi)
    {
        $this->inputApi = $inputApi->authData;
    }

    public function registration(Encode $encode)
    {
        $encode->default = $this->doReg();
    }

    private function doReg()
    {
        $user = R::load(USER_TABLE,$this->inputApi['userId']);
        $company = R::dispense(COMPANY_TABLE);
        $company->name = $this->inputApi['companyName'];
        $company->representative = $user;
        $company->about = $this->inputApi['about'];
        $company->password = password_hash($this->password, PASSWORD_DEFAULT);//Для доступа и изменения данных используется пароль
        $id = R::store($company); //В переменной будет храниться идентификатор под которым он был записан
        return ['status' => 'Success Reg', 'companyName' => $this->inputApi['companyName'], 'YourID' => $id];
        /*Тут будем регистрировать*/
    }
}