<?php
use PHPUnit\Framework\TestCase;
define ('ACCESS_DENIED', 403);
define ('UNAUTHORIZED', 401);
require_once 'Auth.php';
class SetLoginData
{
    public $authData;

    public function getLoginData()
    {
        $this->authData = array('login' => 'test@mail.ru', 'password' => 'qwe123');
    }
}

class AuthTest extends TestCase
{

    /**
     * @throws Exception
     */
    public function testLoginValidate()
    {
        $loginData = new SetLoginData();
        $loginData->getLoginData();
        $class = new api\v2\user\Authorization\Auth($loginData);
        $this->assertEquals(true, $class->loginValidate());
    }

    /**
     * @throws Exception
     */
    public function testDoLogin()
    {
        $loginData = new SetLoginData();
        $loginData->getLoginData();
        $class = new api\v2\user\Authorization\Auth($loginData);
        $result = ['id' => 2, 'name' => 'ivan', 'country' => 'Russia', 'office' => ['yandex', 'management']];
        $this->assertEquals($result, $class->doLogin());
    }

    /**
     * @throws Exception
     */
    public function testLogin() {
        $loginData = new SetLoginData();
        $loginData->getLoginData();
        $class = new api\v2\user\Authorization\Auth($loginData);
        $result = ['id' => 2, 'name' => 'ivan', 'country' => 'Russia', 'office' => ['yandex', 'management']];
        $this->assertEquals($result, $class->login());
    }
}
/*
 * ./vendor/bin/phpunit lib/api/v2/user/Authorization/AuthTest.php
*/