<?php

namespace api\v2\user\Registration;

use Respect\Validation\Validator as v;

class Reg
{
    private $login;
    private $password;

    /**
     * Reg constructor.
     * @param $data
     * @throws \Exception
     */
    public function __construct($data)
    {
        if ($data->authData['repPassword'] !== $data->authData['password']) {
            throw new \RuntimeException('Введенные пароли не совпадают', UNAUTHORIZED);
        }
        $this->login = $data->authData['login'];
        $this->password = $data->authData['password'];
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function registration()
    {
        $this->loginValidate();
        return $this->doReg();
    }


    /**
     * @return string
     * @throws \Exception
     */
    public function loginValidate()
    {

        if (!v::email()->validate($this->login)) {
            throw new \RuntimeException('Не верный формат логина / Не является email адресом', ACCESS_DENIED);
        }
        return true;
    }

    private function doReg()
    {
        return ['status' => 'OK', 'login' => $this->login, 'password' => $this->password];
        /*Тут будем регистрировать*/
    }
}