<?php
namespace api\v2\user\Registration;

use PHPUnit\Framework\TestCase;
define ('ACCESS_DENIED', 403);
define ('UNAUTHORIZED', 401);
require_once 'Reg.php';

class RegTest extends TestCase {
    /**
     * @throws Exception
     * @throws \Exception
     */
    public function testLoginValidate() {
        $loginData = new \api\Reader\Read();
        $account = ['login' => 'test@mail.ru', 'password' => 'qwe123', 'repPassword' => 'qwe123'];
        $loginData->authData = $account;
        $class = new Reg($loginData);
        $this->assertEquals(true, $class->loginValidate());
    }

    /**
     * @throws Exception
     * @throws \Exception
     */
    public function testRegistration() {
        $loginData = new \api\Reader\Read();
        $account = ['login' => 'test@mail.ru', 'password' => 'qwe123', 'repPassword' => 'qwe123'];
        $loginData->authData = $account;
        $class = new Reg($loginData);
        $this->assertEquals(['status' => 'OK', 'login' => 'test@mail.ru'], $class->registration());
    }
    public function testError() {
        $loginData = new \api\Reader\Read();
        try {
            $class = new Reg($loginData);
            $ans = $class->loginValidate();
            $this->assertEquals(true, $ans);
        } catch (\Exception $e) {
            $this->assertEquals(ACCESS_DENIED, $e->getCode());
        }
    }
}
//./vendor/bin/phpunit lib/api/v2/user/Registration/RegTest.php