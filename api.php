<?php
//3 commit
header('Content-Type: application/json');
require_once $_SERVER['DOCUMENT_ROOT'] . '/backend/configs.php';

$className = 'api\\' . $_GET['v'] . '\\' . $_GET['type'] . '\\' . $_GET['library'] . '\\' . $_GET['className'];
$method = $_GET['method'];
$encode = new api\JsonEncode\Encode();
$reader = new api\Reader\Read();
$reader->getJsonDecodeData();
try {
    if (!class_exists($className)) {
        throw new \RuntimeException('Class not found'.$className, ACCESS_DENIED);
    }
    $class = new $className($reader);
    if (!method_exists($class, $method)) {
        throw new \RuntimeException('Method not found', ACCESS_DENIED);
    }
    $class->$method($encode);
} catch (Exception $e) {
    header('error:' . $e->getMessage(), true, $e->getCode());
    $encode->default = ['message' => $e->getMessage(), 'code' => $e->getCode()];
}
echo $encode->toJson();